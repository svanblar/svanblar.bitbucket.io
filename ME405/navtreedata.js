/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Mechatronics Code Documentation", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Motor Driver", "index.html#sec_mot", null ],
    [ "Encoder", "index.html#sec_enc", [
      [ "Purpose", "index.html#sec_purp", null ],
      [ "Testing", "index.html#sec_test", null ],
      [ "Bugs and Limitations", "index.html#sec_bugs", null ],
      [ "Location", "index.html#sec_loc", null ]
    ] ],
    [ "Controller", "index.html#sec_con", null ],
    [ "Tuning Process of the Motor", "page1.html", [
      [ "The Process", "page1.html#sec_process", null ]
    ] ],
    [ "Inertial Measurement Unit (IMU) Operation and Testing", "page2.html", null ],
    [ "Project Proposal", "page3.html", [
      [ "Problem Statement", "page3.html#sec_prob", null ],
      [ "Project Requirement Fulfillment", "page3.html#sec_req", null ],
      [ "Materials", "page3.html#sec_mat", null ],
      [ "Manufacturing Plan", "page3.html#sec_man", null ],
      [ "Safety Assessment", "page3.html#sec_safe", null ],
      [ "Timeline", "page3.html#sec_plan", null ]
    ] ],
    [ "Term Project", "page4.html", [
      [ "Function", "page4.html#sec_func", null ],
      [ "Bugs and Limitations", "page4.html#sec_limit", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';