var annotated_dup =
[
    [ "controller", "namespacecontroller.html", "namespacecontroller" ],
    [ "ds18x20", null, [
      [ "DS18X20", "classds18x20_1_1DS18X20.html", "classds18x20_1_1DS18X20" ]
    ] ],
    [ "encoder", "namespaceencoder.html", "namespaceencoder" ],
    [ "IMUoperator", null, [
      [ "IMUoperator", "classIMUoperator_1_1IMUoperator.html", "classIMUoperator_1_1IMUoperator" ]
    ] ],
    [ "motor", "namespacemotor.html", "namespacemotor" ],
    [ "onewire", null, [
      [ "OneWire", "classonewire_1_1OneWire.html", "classonewire_1_1OneWire" ],
      [ "OneWireError", "classonewire_1_1OneWireError.html", null ]
    ] ],
    [ "tempsens", "namespacetempsens.html", "namespacetempsens" ]
];