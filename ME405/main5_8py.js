var main5_8py =
[
    [ "actuatorsig", "main5_8py.html#a13e16a1f3fc38de7fd4a34256b26124d", null ],
    [ "desiredT", "main5_8py.html#a306f62b16ea65fb6c00c2d08d25f4f8c", null ],
    [ "EN1_A", "main5_8py.html#a7ce2b8d14eede311b85151f77f965b93", null ],
    [ "EN1_B", "main5_8py.html#a14f33986dc07518e580923a696c5705c", null ],
    [ "EN2_A", "main5_8py.html#a15fa2f3722a8718a477e646df6021a7b", null ],
    [ "EN2_B", "main5_8py.html#a368bef7397341c6ca747d593ad70c115", null ],
    [ "enc", "main5_8py.html#a8dfe5fa63c45277d30a8161caff336fa", null ],
    [ "initialduty", "main5_8py.html#a32077d8d2d4d42fe08685f15c5a23563", null ],
    [ "Kp", "main5_8py.html#a9008934a2b727be9889da1474f533612", null ],
    [ "measuredvalue", "main5_8py.html#afee8273e8f1be917a7e9f798150e1b77", null ],
    [ "moe", "main5_8py.html#a6f4ee81f32134fa12a923ee64fc22ee5", null ],
    [ "pcon", "main5_8py.html#a83a9186922f92663276c489ff941a48f", null ],
    [ "pin_EN", "main5_8py.html#affcbdf13952c1b047d9a609e726fe1bf", null ],
    [ "pin_IN1", "main5_8py.html#ab99e86f44420d789102fd9d8aa6f580d", null ],
    [ "pin_IN2", "main5_8py.html#a61fd1cf6703d9b1cc27ab2a3ba411643", null ],
    [ "Setpoint", "main5_8py.html#a4d5e6620133eafeca1b6203dd1de006a", null ],
    [ "t", "main5_8py.html#ab19ee5ab566125a81149ec13ebbbc200", null ],
    [ "temp", "main5_8py.html#acc554f4b967288160fac1d631073a9f4", null ],
    [ "tim", "main5_8py.html#ad4a0673eb46391b1d764c9c74804c223", null ],
    [ "tim4", "main5_8py.html#a9da3e397586167e8443cdab3fb7b640d", null ],
    [ "tim8", "main5_8py.html#a9cb5b42eff40a6d5e0e327eab77cbdd8", null ]
];