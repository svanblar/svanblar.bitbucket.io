var main3_8py =
[
    [ "actuatorsig", "main3_8py.html#a401542e4a7f23fee306a359a4cb07d47", null ],
    [ "EN1_A", "main3_8py.html#ad4f50b94f99f843c03e4fc4e94032553", null ],
    [ "EN1_B", "main3_8py.html#a967c4e02c555c29e9312fb832d6f9558", null ],
    [ "EN2_A", "main3_8py.html#ad340aeb0fc6d54d2d24c2cd0ca6ef0e3", null ],
    [ "EN2_B", "main3_8py.html#a6bb5fea3f3ae1d7b317bb9fd71cc6479", null ],
    [ "enc", "main3_8py.html#a4515e162e699f9da432e7845823c59db", null ],
    [ "initialduty", "main3_8py.html#a69817c541fc7038216ef9e7668724e28", null ],
    [ "Kp", "main3_8py.html#a3987500f0de4250283a521b3ff476edc", null ],
    [ "list_position", "main3_8py.html#a87516b2a8c9c05907feaf93edb8aadd5", null ],
    [ "list_time", "main3_8py.html#a6c24743557639195091acb6b9e4eab28", null ],
    [ "measuredtime", "main3_8py.html#a38e2350559359d48ddb796e3ad5cf0a4", null ],
    [ "measuredvalue", "main3_8py.html#a09db8d3235c5b762b8711a1190705ff8", null ],
    [ "moe", "main3_8py.html#a9848c84c7641daf164bc8409cf675d7c", null ],
    [ "pcon", "main3_8py.html#a0ea923ff732473529e26dc5056581e46", null ],
    [ "pin_EN", "main3_8py.html#a43b9d478382e26057b529cca98eed15b", null ],
    [ "pin_IN1", "main3_8py.html#a9f1dd84599dcc751657f12981b7a7645", null ],
    [ "pin_IN2", "main3_8py.html#a9a9debeb95d78dcfccc5d3b2020a41b1", null ],
    [ "Setpoint", "main3_8py.html#adad33b4fa60cd12c60b48374add99e7a", null ],
    [ "tim", "main3_8py.html#ab2d795590304904790c950f98410bc29", null ],
    [ "tim4", "main3_8py.html#a53deb84c3d694e53231037bc6f13ea6a", null ],
    [ "tim8", "main3_8py.html#a539f9cbbe5aad385a97f5e25ddfd66ee", null ]
];